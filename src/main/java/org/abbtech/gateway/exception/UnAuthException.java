package org.abbtech.gateway.exception;

public class UnAuthException extends RuntimeException{
    public UnAuthException(String msg){
        super(msg);
    }
}
