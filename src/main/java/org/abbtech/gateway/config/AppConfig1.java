package org.abbtech.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig1 {

    @Bean
    public RestTemplate template(){
        return new RestTemplate();
    }
}