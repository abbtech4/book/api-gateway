package org.abbtech.gateway.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GeneralExceptionHandler {

    @ExceptionHandler(UnAuthException.class)
    public ResponseEntity<String> UnAuthRequest(UnAuthException unAuthException){
        return new ResponseEntity<>(unAuthException.getMessage(), HttpStatus.FORBIDDEN);
    }
}
